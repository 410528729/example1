﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _502.Controllers
{
    public class scoreController : Controller
    {
        // GET: score
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Index(int score)
        {

            string level = "";
            if (score < 20 && score >= 0)
            {
                level = "E";
            }
            else if (20 <= score && score < 40)
            {
                level = "D";
            }
            else if (40 <= score && score < 60)
            {
                level = "C";
            }
            else if (60 <= score && score < 80)
            {
                level = "B";
            }
            else if (80 <= score && score <= 100)
            {
                level = "A";
            }
            ViewBag.Score = score;
            ViewBag.level = level;
            return View();
        }

    }
    

}