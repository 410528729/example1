﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _502.ViewModel
{
    public class BMIData
    {
        [Required(ErrorMessage = "required!!")]
        [Range(30,150 ,ErrorMessage ="請輸入30-150的數值")]
        [Display(Name ="體重")]
        public float Weight { get; set; }

        [Required(ErrorMessage = "required!!")]
        [Range(50, 200, ErrorMessage = "請輸入50-200的數值")]
        [Display(Name = "身高")]
        public float Height { get; set; }
        public float BMI { get; set;  }
        public string Level { get; set; }
    }
}