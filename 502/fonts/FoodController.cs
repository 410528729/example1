﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _502.fonts
{
    public class FoodController : Controller
    {
        // GET: Food
        public ActionResult Index()
        {
            String[] fruit = { "蘋果", "香蕉", "荔枝", "鳳梨", "西瓜" };

            ViewBag.Data = fruit;
            ViewData["Data"] = fruit;
            return View();
        }
    }
}